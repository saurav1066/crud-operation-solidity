pragma solidity ^0.5.0;

/*
    *Array is a data structure, which stores a fixed size sequential collection of elements of the same type.
    *used to store a collection of data
    *In solidity an array can be of compile time fixed size or dynamic size.
    *For storage array, it can have different types of elements as well.
    *in case of memory array, element type cannot be mapping and in case it is to be used as function parameter then element type should be as ABI type
    *declaring array: type arrayname [ arraysize] - single dimensional array;
*/

contract ArrayImpl{
    function Aray() public pure returns(string memory){
        string memory a = "";
        string[2] memory aray = ["hello", "hi"];
        for(uint i = 0; i<aray.length;i++){
            a = aray[i];
        }
        return a;

    }
}
