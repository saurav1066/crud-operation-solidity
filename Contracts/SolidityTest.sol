pragma solidity ^0.5.11;
contract SolidityTest{

    /* pure means a pure function, which is a function to return a value using only the parameter of the function without any side effects*/
    function getResult() public pure returns(uint){
        uint a = 1;
        uint b = 2;
        uint result = a + b;
        return result;
    }
}

/*
Global Variables i Solidity
    * blockhash(uint blockNumber) returns(bytes32) - hash of the given block
    * block.coinbase(address payable) - returns Current block miner's address
    * block.difficulty(uint)- returns current block difficulty
    * block.gaslimit(uint) - returns block gas limit
    * block.number(uint) - returns Current block number
    * block.timestamp(uint) - returns current block timestamp as seconds since unix epoch
    * gasleft() returns (uint256) - returns Remaining gas
    * msg.data(bytes calldata) - returns Complete calldata
    * msg.sender(address payable) - returns Sender of the message(current caller)
    * msg.sig(bytes4) - returns First 4 bytes of the calldata(function identifier)
    * msg.value(uint) - returns Number of wei with the message
    * now(uint) - returns Corrent block timestamp
    * tx.gasprice(uint) - returns Gas price of the transactions
    * tx.origin(address payable) - returns sender of the transaction

*/