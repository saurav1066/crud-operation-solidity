/* ---state variable scope---
    * public-can be accessed internally as well as via mesg
    * internal-only internally from the current contract or the contract derievng from it
    * private- internally only from the current contract
*/
pragma solidity ^0.5.0;
contract C{

    uint public data = 30; //state variable//
    uint internal iData = 10; //state variable//

    function x() public returns (uint){
        data = 3; //internal access//
        return data;
    }
}

contract Caller {
    C c = new C();
    function f() public view returns (uint){
        return c.data(); //external access//
    }
}
// C is the parent D is the child i.e D inherits C//
contract D is C {
    function y() public returns (uint){
        iData = 3;
        return iData;
    }
    function getResult() public pure returns(uint){
        uint a = 1;
        uint b = 2;
        uint result = a + b;
        return result;
    }
}
