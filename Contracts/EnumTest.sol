pragma solidity ^0.5.0;

//Enum restricts a variable to have one of only few predefined values//
//Use of enums reduces the number of bugs in the code//

contract EnumTest {
    enum FreshJuiceSize{ SMALL, MEDIUM, LARGE}
    FreshJuiceSize choice;
    FreshJuiceSize constant defaultChoice = FreshJuiceSize.MEDIUM;

    function setLarge() public {
        choice = FreshJuiceSize.LARGE;
    }

    function getChoice() public view returns (FreshJuiceSize){
        return choice;
    }

    function getDefault() public pure returns (uint){
        return uint(defaultChoice);
    }
}