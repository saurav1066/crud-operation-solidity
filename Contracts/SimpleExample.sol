pragma solidity ^0.5.0; //Solidity Version 0.5.0 or above//

/*
    *Contract is a collection of function and its state(data)
    *declaration of a state variable of type unsigned integer
*/
contract SimpleExample{
    uint storedData;

    /*
        *function set to modify the value of a variable
    */
    function set(uint x) public{
        storedData = x;
    }

    /*
        *function get to get the value stored in the variable
        *View means that the function will not modify the state of the contract */
    function get() public view returns (uint){
        return storedData;
    }
}