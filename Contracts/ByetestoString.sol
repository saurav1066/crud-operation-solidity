pragma solidity ^0.5.0;
contract BytestoString {
    string message = "hello";
    function getResult() public returns(string memory){
        bytes memory bstr = new bytes(5);
        message = string(bstr);
        return message;
    }
}